# PoidsIdeal

Webapp permettant de calculer son poids idéal en fonction de sa taille. Ecrit en Javascript & Bootstrap.

Démo disponible ici : [https://extrem-network.com/dev/projects/poids-ideal](https://extrem-network.com/dev/projects/poids-ideal)